call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'ycm-core/YouCompleteMe'
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'kien/ctrlp.vim'
Plug 'rking/ag.vim'
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}
"colorchemes
Plug 'morhetz/gruvbox'

call plug#end()


" turn on syntax highlight
syntax on
" customize theme
let g:mapleader=','
colorscheme gruvbox
set background=dark
set number

" tabs 
autocmd FileType sh setlocal ts=4 sts=4 sw=4
autocmd FileType cpp setlocal ts=2 sts=2 sw=2
autocmd FileType javascript setlocal ts=2 sts=2 sw=2
autocmd FileType python setlocal ts=4 sts=4 sw=4
autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType css setlocal ts=4 sts=4 sw=4

" Перед сохранением вырезаем пробелы на концах (только в .py файлах)
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
" В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class


" turn off autopairs and tabs
set pastetoggle=<F3>

" set nu "Включаем нумерацию строк
set mousehide "Спрятать курсор мыши когда набираем текст
set mouse=a "Включить поддержку мыши
set termencoding=utf-8 "Кодировка терминала
set novisualbell "Не мигать 
set t_vb= "Не пищать! (Опции 'не портить текст', к сожалению, нету)
" Удобное поведение backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]
" Вырубаем черточки на табах
set showtabline=1

" Переносим на другую строчку, разрываем строки
" set wrap
" set linebreak

set nowrap           " do not automatically wrap on load
set formatoptions-=t " do not automatically wrap text when typing

set hlsearch
set incsearch

" Вырубаем .swp и ~ (резервные) файлы
set nobackup
set noswapfile
set encoding=utf-8 " Кодировка файлов по умолчанию
set fileencodings=utf8,cp1251

"mappings
map <C-n> :NERDTreeToggle<CR>
map <Leader> <Plug>(easymotion-prefix)
 
"copy to global clipboard
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y
vnoremap <C-d> "+d

"new line without insert mode
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc>

"for alt works correctly
execute "set <M-e>=\ee"
nnoremap <M-e> e
execute "set <M-b>=\eb"
nnoremap <M-b> b

" add spaces in normal mode
nnoremap <space> i<space><esc>

"auto pairing 
let g:AutoPairsFlyMode = 1
let g:AutoPairsShortcutBackInsert = '<M-b>'
"let g:AutoPairsShortcutToggle = 

