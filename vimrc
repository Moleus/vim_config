" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark




"""""""""""""""""""""""""""""""""""""""""
" buka prefered profile config

autocmd VimEnter 'syndaemon -i 1 -K -d'

"
" turn on syntax highlight
syntax on
" customize theme
let g:mapleader=','
colorscheme "/usr/share/vim/vim74/colors/gruvbox.vim" 
set background=dark
set number

" tabs and comments pluging
autocmd FileType sh setlocal ts=4 sts=4 sw=4 commentstring=#\ %s
autocmd FileType cpp setlocal ts=2 sts=2 sw=2 commentstring=//\ %s
autocmd FileType javascript setlocal ts=2 sts=2 sw=2 commentstring//\ %s
autocmd FileType python setlocal ts=4 sts=4 sw=4 commentstring=#\ %s
autocmd FileType html setlocal ts=2 sts=2 sw=2 commentstring=<!--\%s-->
autocmd FileType css setlocal ts=4 sts=4 sw=4 commentstring=/*\ %s */

" Перед сохранением вырезаем пробелы на концах (только в .py файлах)
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
" В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class


" turn off autopairs and tabs
set pastetoggle=<F3>

" set nu "Включаем нумерацию строк
set mousehide "Спрятать курсор мыши когда набираем текст
set mouse=
set ttymouse=
set termencoding=utf-8 "Кодировка терминала
set novisualbell "Не мигать 
set t_vb= "Не пищать! (Опции 'не портить текст', к сожалению, нету)
" Удобное поведение backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]
" Вырубаем черточки на табах
set showtabline=1

" Переносим на другую строчку, разрываем строки
" set wrap
" set linebreak

set nowrap           " do not automatically wrap on load
set formatoptions-=t " do not automatically wrap text when typing

set hlsearch
set incsearch

" Вырубаем .swp и ~ (резервные) файлы
set nobackup
set noswapfile
set encoding=utf-8 " Кодировка файлов по умолчанию
set fileencodings=utf8,cp1251

"mappings
map <C-n> :NERDTreeToggle<CR>
map <Leader> <Plug>(easymotion-prefix)
 
"copy to global clipboard
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y
vnoremap <C-d> "+d

"new line without insert mode
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc>

"for alt works correctly
execute "set <M-e>=\ee"
nnoremap <M-e> e
execute "set <M-b>=\eb"
nnoremap <M-b> b

" add spaces in normal mode
nnoremap <space> i<space><esc>

"auto pairing 
let g:AutoPairsFlyMode = 1
let g:AutoPairsShortcutBackInsert = '<M-b>'
"let g:AutoPairsShortcutToggle = 


" end of buka config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""''''
"
" Uncomment the following to have Vim jump to the last position when
" reopening a file
"au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"filetype plugin indent on

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
"set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden		" Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif
